#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <list>
#include <thread>
#include <iostream>
#include <sstream>
#include <cstring>
#include <map>

using namespace std;

int socket_descriptor;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
list<string> threads;
list<thread> threads_list;
map<string, int> threads_map;

string list_to_string() {
    struct sockaddr_in sai{};
    socklen_t len = sizeof(sai);
    string threadsList = "list of threads: ";
    for (const string &v : threads) {
        threadsList += v;
        threadsList += " ";
    }
    threadsList += "\n";
    return threadsList;
}

int readn(int fd, char *bp, size_t len) {
    int cnt;
    int rc;

    cnt = len;
    while (cnt > 0) {
        rc = recv(fd, bp, cnt, 0);
        if (rc < 0) {
            if (errno == EINTR)
                continue;
            return -1;
        }
        if (rc == 0)
            return len - cnt;
        bp += rc;
        cnt -= rc;
    }
    return len;
}

list<string> split(const string &s, char delimiter) {
    list<string> tokens;
    string token;
    istringstream token_stream(s);
    while (getline(token_stream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

string to_str(thread::id id) {
    stringstream ss;
    ss << id;
    return ss.str();
}

bool contains_thread(const string &id) {
    for (const string &elem : threads) {
        if (strcmp(elem.c_str(), id.c_str()) == 0) {
            return true;
        }
    }
    return false;
}

void join_by_id(const string &id) {
    for (thread &elem : threads_list) {
        if (strcmp(to_str(elem.get_id()).c_str(), id.c_str()) == 0) {
            elem.join();
        }
    }
}

void fun() {
    string first_string = "Hi ";
    first_string += to_str(this_thread::get_id());
    int local_socket_descr = threads_map[to_str(this_thread::get_id())];
    write(local_socket_descr, first_string.c_str(), 1024);
    while (true) {
        string message;
        char buf[1024];
        int bytes_read;
        if (!contains_thread(to_str(this_thread::get_id()))) {
            break;
        }
        bytes_read = readn(local_socket_descr, buf, 1024);
        if (bytes_read <= 0) {
            break;
        }
        if (!contains_thread(to_str(this_thread::get_id()))) {
            break;
        }
        write(local_socket_descr, buf, 1024);
    }
    shutdown(threads_map[to_str(this_thread::get_id())], 2);
    close(local_socket_descr);
    pthread_mutex_lock(&mutex);
    threads.remove(to_str(this_thread::get_id()));
    pthread_mutex_unlock(&mutex);
}

void kill(const string &id) {
    if (contains_thread(id)) {
        shutdown(threads_map[id], 2);
        close(threads_map[id]);
        join_by_id(id);
        pthread_mutex_lock(&mutex);
        threads_map.erase(threads_map.find(id));
        threads.remove(id);
        pthread_mutex_unlock(&mutex);
        printf("Killed %s\n", id.c_str());
    } else {
        printf("Can not kill this\n");
    }
}

void exit() {
    auto it = threads_map.begin();
    for (int i = 0; it != threads_map.end(); it++, i++) {  // выводим их
        shutdown(it->second, 2);
        close(it->second);
        threads.remove(it->first);
    }
    shutdown(socket_descriptor, 2);
    close(socket_descriptor);
    printf("%s", "EXIT\n");
}

void admin_fun() {
    char buffer[1024];
    while (true) {
        cin.getline(buffer, 1024, '\n');
        if (strcmp(buffer, string("list").c_str()) == 0) {
            printf("%s", list_to_string().c_str());
        } else if (strcmp(buffer, string("exit").c_str()) == 0) {
            exit();
            break;
        } else {
            list<string> listSplit = split(buffer, ' ');
            if (listSplit.size() > 1) {
                if (strcmp(listSplit.front().c_str(), string("kill").c_str()) == 0) {
                    listSplit.pop_front();
                    kill(listSplit.back());
                }
            } else {
                printf("wrong argument\n");
            }
        }
    }
}

int main(int argc, char *argv[]) {
    int new_socket_descriptor;
    struct sockaddr_in addr{};
    socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_descriptor < 0) {
        perror("socket");
        exit(1);
    }
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[1]));
    addr.sin_addr.s_addr = INADDR_ANY;
    const int on = 1;
    setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (bind(socket_descriptor, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(1);
    }
    listen(socket_descriptor, 1);
    thread t0(admin_fun); //listen command
    while (true) {
        new_socket_descriptor = accept(socket_descriptor, nullptr, nullptr);
        if (new_socket_descriptor < 0) {
            break;
        }
        pthread_mutex_lock(&mutex);
        threads_list.emplace_back([&] { fun(); });
        string thread_id = to_str(threads_list.back().get_id());
        threads_map[thread_id] = new_socket_descriptor;
        threads.push_back(thread_id);
        pthread_mutex_unlock(&mutex);
    }
    for (thread &t: threads_list) {
        t.join();
    }
    t0.join();
    return 0;
}